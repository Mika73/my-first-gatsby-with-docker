# Use the official Node.js 18 image as the base image
FROM node:alpine 

# Set the working directory inside the container to /app
WORKDIR /app

# Copy the content of the current directory (including Dockerfile) into the /app directory of the container
COPY . /app

# Run npm install to install dependencies specified in package.json
RUN npm install

# Set PATH to include the directory with the Gatsby executable
ENV PATH=$PATH:/app/node_modules/.bin

# Define the default command to run when the container starts
CMD ["gatsby", "develop", "-H", "0.0.0.0"]
