# A Gatsby project set up with a Docker environment

## How to manually start Gatsby with Docker
1. Use node base image instead of node:alpine image.
1. Build a docker image. `docker build -t gatsby-dev .`
1. Run the container from the Docker image in detached mode. `docker run -dit --rm --name gatsby-dev -p 8000:8000 -v ${PWD}/src:/app/src -v ${PWD}/gatsby-config.js:/app/gatsby-config.js gatsby-dev`
1. Run a shell inside a Docker container. `docker exec -it gatsby-dev /bin/bash`
1. Start gatsby `gatsby develop -H 0.0.0.0`
1. Using your web browser, navigate to http://localhost:8000/
1. To explore your site's data, go to http://localhost:8000/___graphql in your browser.
1. To stop running the development server, ctrl-c. 

## How to start Gatsby with Docker composer
1. Start the service and build the images if they don't exist or if changes are detected in the build context 
`docker-compose up --build`
1. Stop the container. `docker-compose down`

## useful Docker commands for development
1. Check the container logs. `docker logs gatsby-dev`
1. Stop the container. `docker stop gatsby-dev`
1. Remove the image. `docker rmi gatsby-dev`
1. List Running Containers `docker ps`
1. List All Containers (including stopped ones) `docker ps -a`
