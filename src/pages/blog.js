import * as React from "react"
import {graphql} from "gatsby"

const Blog = (props) => {
   return (
    <div>
      <h1>This is blog page !!!</h1>
      {props.data.allMarkdownRemark.edges.map((singleBlog, index)=>{
        <div>
          <div>{singleBlog.node.frontmatter.title}</div> 
          <div>Hello</div>
        </div>
      })}
    </div> 
   ) 
  }
export default Blog

export const query = graphql`query MyQuery {
  allMarkdownRemark {
    edges {
      node {
        frontmatter {
          id
          title
          author
          date
          categories
          tags
        }
      }
    }
  }
}`
