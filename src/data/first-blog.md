---
id: "1"
title: "Sample Markdown File1"
author: "Naruto Uzumaki"
date: "2024-01-01"
categories:
  - Technology
tags:
  - Markdown
  - Front Matter
---

# Content starts here
This is the main content of your Markdown file.
